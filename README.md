# learning-from-date

Machine Learning [library `scikit-learn`](https://scikit-learn.org/stable/user_guide.html)


## Environment installation

```sh
python3 -m venv venv
source ./venv/bin/activate.fish

pip install --upgrade pip
pip install jupyterlab
pip install -U scikit-learn
pip install bpython

# List available kernels
jupyter kernelspec list
jupyter lab &
```

#### on Ubuntu

```
sudo apt-get install python3-sklearn python3-sklearn-lib python3-sklearn-doc
```

## Model Persistance

```python
from sklearn import svm
from sklearn import datasets

clf = svm.SVC()
X, y = datasets.load_iris(return_X_y=True)
clf.fit(X, y)

import pickle
s = pickle.dumps(clf)
clf2 = pickle.loads(s)
clf2.predict(X[0:1])
y[0]
```

### Resources

1. https://chrisalbon.com/machine_learning/basics/perceptron_in_scikit-learn/
2. 